package ltd.newbee.mall.service.impl;


import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import ltd.newbee.mall.entity.NewBeeMallGoods;
import ltd.newbee.mall.service.ISearchService;
import ltd.newbee.mall.util.PageResult;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.elasticsearch.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.*;

@Slf4j
@Service
public class ISearchServiceImpl implements ISearchService {

    @Autowired
    private RestHighLevelClient client;


    @Override
    public PageResult<NewBeeMallGoods> search(Map<String,String> params) throws IOException {
        SearchRequest request = new SearchRequest("goods");
        request.source(SearchSourceBuilder.searchSource().fetchSource( new String[]{"goodsId","goodsName","goodsIntro","goodsCategoryId","goodsCoverImg","goodsCarousel",
                "goodsDetailContent","originalPrice","sellingPrice","stockNum","tag","goodsSellStatus",
                "createUser","createTime","updateUser","updateTime"},null));

        BoolQueryBuilder boolQuery=QueryBuilders.boolQuery();
//                    boolQuery.must(QueryBuilders.matchAllQuery());

        // 关键字搜索
        if (StringUtils.isEmpty(params.get("keyword"))) {
            boolQuery.must(QueryBuilders.matchAllQuery());
        } else {
            Map<String,Float> fieldsWeightMap=new HashMap<>();
            fieldsWeightMap.put("goodsName",1000000f);
            fieldsWeightMap.put("goodsIntro",100000f);
            fieldsWeightMap.put("goodsDetailContent",10000f);
            fieldsWeightMap.put("tag",2000f);
            boolQuery.must(QueryBuilders.multiMatchQuery( params.get("keyword")).fields(fieldsWeightMap));
        }

        // 条件过滤
        if (params.get("goodsCategoryId")!=null && !params.get("goodsCategoryId").equals("null")){
            boolQuery.filter(QueryBuilders.termsQuery("goodsCategoryId",params.get("goodsCategoryId")));
        }
//        if (params.getLanguageId()!=null && params.getLanguageId().size()>0){
//            boolQuery.filter(QueryBuilders.termsQuery("languageId",params.getLanguageId()));
//        }
//        if (params.getDepartmentalLawCategory()!=null && params.getDepartmentalLawCategory().size()>0){
//            boolQuery.filter(QueryBuilders.termsQuery("departmentalLawCategory",params.getDepartmentalLawCategory()));
//        }
//        if (params.getCountryIds()!=null && params.getCountryIds().size()>0){
//            boolQuery.filter(QueryBuilders.termsQuery("countryId",params.getCountryIds()));
//        }
        boolQuery.filter(QueryBuilders.termsQuery("goodsSellStatus",new int[]{0}));

        request.source().query(boolQuery);
        String preTag = "<span style='color:red;font-weight:bold'>";
        String postTag = "</span>";
        request.source().highlighter(new HighlightBuilder().field("goodsName")
                .requireFieldMatch(false)
                .preTags(preTag)
                .postTags(postTag));

        Integer pageNum = Integer.valueOf(params.get("pageNum"));
        Integer pageSize = Integer.valueOf(params.get("pageSize"));
        if (pageNum != null && pageSize != null) {
            request.source().from((pageNum - 1) * pageSize).size(pageSize);
        }

        if (params.get("orderBy")!=null && params.get("orderBy").length()>0 && !params.get("orderBy").equals("null")){
            FieldSortBuilder sortBuilder=new FieldSortBuilder(params.get("orderBy")).order(SortOrder.ASC);
            request.source().sort(sortBuilder);
        }

        SearchResponse searchResponse = client.search(request, RequestOptions.DEFAULT);
        return handleResponse(searchResponse,params);
    }

    @Override
    public IndexResponse insertDoc(NewBeeMallGoods goods) throws IOException {
        log.info("ISearchService.insertDoc params:{}", JSON.toJSONString(goods));
        IndexRequest request=new IndexRequest("goods").id(goods.getGoodsId()+"");
        goods.setUpdateTime(null);
        goods.setCreateTime(null);
        request.source(JSON.toJSONString(goods), XContentType.JSON);
        return client.index(request, RequestOptions.DEFAULT);
    }

    @Override
    public UpdateResponse updateDoc(NewBeeMallGoods goods) throws IOException {
        log.info("ISearchService.updateDoc params:{}",JSON.toJSONString(goods));
        UpdateRequest request=new UpdateRequest("goods",goods.getGoodsId()+"");
        goods.setUpdateTime(null);
        goods.setCreateTime(null);
        request.doc(JSON.toJSONString(goods), XContentType.JSON);
        UpdateResponse updateResponse = client.update(request, RequestOptions.DEFAULT);
        return updateResponse;
    }

    @Override
    public DeleteResponse deleteDoc(NewBeeMallGoods goods) throws IOException {
        log.info("ISearchService.deleteDoc params:{}",JSON.toJSONString(goods));
        DeleteRequest request = new DeleteRequest("goods",goods.getGoodsId()+"");
        DeleteResponse response = client.delete(request, RequestOptions.DEFAULT);
        return response;
    }

    private PageResult<NewBeeMallGoods> handleResponse(SearchResponse response, Map<String, String> params) {


        SearchHits searchHits = response.getHits();
        long total = searchHits.getTotalHits().value;

        List<NewBeeMallGoods> list = new ArrayList<>();
        for (SearchHit hit : searchHits) {
            String json = hit.getSourceAsString();
            NewBeeMallGoods goods = JSON.parseObject(json, NewBeeMallGoods.class);
//            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
//            if (!highlightFields.isEmpty() && highlightFields.get("goodsName")!=null)
//                goods.setGoodsName(highlightFields.get("goodsName").getFragments()[0].string());
            list.add(goods);
        }
        PageResult<NewBeeMallGoods> pageResult = new PageResult<>(list,(int)total,
                Integer.parseInt(params.get("pageSize")),Integer.parseInt(params.get("pageNum")));

        return pageResult;
    }
}
