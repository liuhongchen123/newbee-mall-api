package ltd.newbee.mall.service;



import ltd.newbee.mall.api.mall.vo.NewBeeMallSearchGoodsVO;
import ltd.newbee.mall.entity.NewBeeMallGoods;
import ltd.newbee.mall.util.PageResult;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateResponse;

import java.io.IOException;
import java.util.Map;

public interface ISearchService {

    PageResult<NewBeeMallGoods> search(Map<String,String> params) throws IOException;

    IndexResponse insertDoc(NewBeeMallGoods goods) throws IOException;

    UpdateResponse updateDoc(NewBeeMallGoods goods) throws IOException;

    DeleteResponse deleteDoc(NewBeeMallGoods goods) throws IOException;
}
