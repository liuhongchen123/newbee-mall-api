package ltd.newbee.mall.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EsConfig {

//    @Value("${spring.elasticsearch.rest.uris}")
    private String uris="http://8.134.160.235:19200";

    @Bean
    public RestHighLevelClient restHighLevelClient(){
        System.out.println(uris);
        return new RestHighLevelClient(RestClient.builder(
//                HttpHost.create("http://8.134.187.137:19200")
                HttpHost.create(uris)
        ));
    }
}
